import scrapy


class HomeSpider(scrapy.Spider):
    name = "home"

    def start_requests(self):
        urls = ['https://discuss.atom.io/']

        for url in urls:
            yield scrapy.Request(url=url, callback=self.parse)

    def parse(self, response):
        filename = 'atom-home.html'
        with open(filename, 'wb') as f:
            f.write(response.body)
        self.log('Saved file %s' % filename)
