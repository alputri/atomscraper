import scrapy


class ContentAndCommentSpider(scrapy.Spider):
    name = "contentandcomment"

    def start_requests(self):

        url_file = 'url.txt'
        file = open(url_file, "w")
        file.close()

        thread_comments_file = 'thread-comments.csv'
        file = open(thread_comments_file, "w")
        file.close()

        url = 'https://discuss.atom.io'
        yield scrapy.Request(url=url, callback=self.parse)

    def parse(self, response):

        if (len(response.url.split("/")) > 3) and (response.url.split("/")[3] == 't'):

            thread_num = response.url.split("/")[-1]

            # filename = 'thread-html-%s.html' % thread_num
            # with open(filename, 'wb') as f:
            #     f.write(response.body)
            # self.log('Saved file %s' % filename)

            # Write url list to a txt file
            url_file = 'url.txt'
            with open(url_file, 'a+') as f:
                # Move read cursor to the start of file.
                f.seek(0)
                # If file is not empty then append '\n'
                data = f.read(100)
                if len(data) > 0:
                    f.write("\n")
                # Append text at the end of file
                f.write(response.url)

            # Write comments to a csv file
            thread_comments_file = 'thread-comments.csv'
            with open(thread_comments_file, 'a+') as f:

                # Comment scraper
                comments = response.css(".post").css("p").getall()
                comments.insert(0, thread_num)

                f.seek(0)
                data = f.read(100)
                if len(data) > 0:
                    f.write("\n")
                f.write(str(comments).strip('[]'))

            self.log('Saved file %s' % thread_comments_file)

        # Thread URL scraper
        anchors = response.css('tr.topic-list-item a')
        yield from response.follow_all(anchors, callback=self.parse)
